<?php
//ini_set('display_errors','on');

$mysqli = new mysqli('localhost', 'root', '', 'tournament');

require_once('./utils/func.php');

define("OPERATIONS_PATH", "./operations/");

// get HTTP request method
$method = $_SERVER['REQUEST_METHOD'];

if ($method === 'GET') {
    // get the desired operation/resource which comes from .htaccess
    switch($_GET['operation']) {
        case 'fund':
            require_once(OPERATIONS_PATH.'fund.php');
            break;
        case 'take':
            require_once(OPERATIONS_PATH.'take.php');
            break;
        case 'announceTournament':
            require_once(OPERATIONS_PATH.'announce_tournament.php');
            break;
        case 'joinTournament':
            require_once(OPERATIONS_PATH.'join_tournament.php');
            break;
        case 'balance':
            require_once(OPERATIONS_PATH.'balance.php');
            break;
        case 'reset':
            require_once(OPERATIONS_PATH.'reset.php');
            break;
        default:
            http_response_code(404);
            die();
    }
} else if ($method === 'POST') {
    if ($_GET['operation'] == 'resultTournament') {
        require_once(OPERATIONS_PATH.'result_tournament.php');
    } else {
        http_response_code(404);
        die();
    }
} else {
    http_response_code(501);
    die();
}
