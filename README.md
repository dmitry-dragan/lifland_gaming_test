# Dmitrijs Dragans test task for Lifland gaming

LAMP (Linux + Apache + Mysql + PHP) stack was used to complete the task. 

[tutum/lamp](https://hub.docker.com/r/tutum/lamp/) Docker image was used as a base image for creating the [dmitrijsdragans/lifland_test_task](https://hub.docker.com/r/dmitrijsdragans/lifland_test_task/) image.

## Setup
 * Download [docker-compose.yml](https://bitbucket.org/dmitry-dragan/lifland_gaming_test/downloads/docker-compose.yml) from [Downloads](https://bitbucket.org/dmitry-dragan/lifland_gaming_test/downloads/).
 * Open Docker Quick Terminal and change the derictory where downloaded docker-compose.yml file is resided
 * Type following in docker terminal:
 
 ```
    docker-compose up
 ```
 
This will spin up an Apache server on port 80, MySQL on port 3306 and PhpMyAdmin panel on port 8080. To change ports edit docker-compose.yml file.
 
*Please find **Dockerfile** in [Downloads](https://bitbucket.org/dmitry-dragan/lifland_gaming_test/downloads/) if needed.*

## PhpMyAdmin credentials:

**Username**: admin

**Password**: lifland