<?php

// Example: {"tournamentId": "1", "winners": [{"playerId": "P1", "prize": 500}]}
// get request payload string and decode JSON to associative array
$request_payload = json_decode(file_get_contents('php://input'), true);

$tournamentData = getTournamentData($request_payload['tournamentId']);

if (!$tournamentData) {
    http_response_code(409);
    die('tournament is not announced'); // tournament is not announced
}

if ($tournamentData['closed'] === 'Y') {
    http_response_code(409);
    die('tournament is closed already'); // tournament is closed already
}

foreach($request_payload['winners'] as $winner) {
    $winPlayerData = getPlayerOfTournament($request_payload['tournamentId'], $winner['playerId']);

    if ($winPlayerData) {
        $backersIdArray = getBackers($winPlayerData['player_reg_id']);

        $prize_share = $winner['prize'] / (count($backersIdArray) + 1);

        topUpPlayersBalance($winner['playerId'], $prize_share);

        foreach($backersIdArray as $backerId) {
            topUpPlayersBalance($backerId, $prize_share);
        }
    }
}

closeTournament($request_payload['tournamentId']);
