<?php

/*
 *   Get array of backer IDs from query string.
 *   PHP expects arrays to be passed in query string as following (with square brackets after the param name): ?tournamentId=1&playerId=P1&backerId[]=P2&backerId[]=P3
 *   PHP doesn't recognize "backerId" as an array in a query string like this: ?tournamentId=1&playerId=P1&backerId=P2&backerId=P3. It instead just takes the last value in query string.
*/
$paramsArray = explode('&', $_SERVER["QUERY_STRING"]);
$backerIdArray = [];
foreach ($paramsArray as $kvPair)
{
    list($paramName, $value) = explode("=", $kvPair);
    if ($paramName === 'backerId') {
        array_push($backerIdArray, $value);
    }
}

registerParticipantsInTournament($_GET['tournamentId'], $_GET['playerId'], $backerIdArray);




