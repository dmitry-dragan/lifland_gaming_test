<?php

$playerIdString = $_GET['playerId'];
$playerBalance = getPlayerBalance($playerIdString);

echo json_encode(array(
    "playerId" => $playerIdString,
    "balance" => $playerBalance
));
