<?php

$sql = "SHOW TABLES FROM `tournament`";

if ($result = $mysqli->query($sql)) {
    while($row = $result->fetch_array()) {
        $sql = "TRUNCATE TABLE ".$row[0];
        if ($mysqli->query($sql) !== TRUE) {
            http_response_code(500);
            die($mysqli->error);
        }
    }
} else {
    http_response_code(500);
    die($mysqli->error);
}