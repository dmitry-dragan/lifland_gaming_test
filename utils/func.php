<?php

function getPlayerIdFromString($playerId) {
    preg_match('/^P(\d+)$/si', $playerId, $matches);

    if (!isset($matches[1])) {
        http_response_code(422); // invalid player ID
        die('Invalid player ID');
    }

    return $matches[1];
}

function getTournamentId($tournamentId) {
    preg_match('/^(\d+)$/s', $tournamentId, $matches);

    if (!isset($matches[1])) {
        http_response_code(422);
        die('Invalid tournament ID');
    }

    return $matches[1];
}

function getTournamentData($tournamentId) {
    global $mysqli;

    $tournamentId = getTournamentId($tournamentId);

    $sql = "SELECT * FROM `tournaments` WHERE tournament_id = ".intval($tournamentId);
    if ($result = $mysqli->query($sql)) {
        $row = $result->fetch_array();
    } else {
        http_response_code(500);
        die($mysqli->error);
    }

    return $row;
}

function getPlayerBalance($playerIdString) {
    global $mysqli;

    $playerId = getPlayerIdFromString($playerIdString);

    $sql = "SELECT points FROM `players` WHERE player_id = ".intval($playerId);
    if ($result = $mysqli->query($sql)) {
        $row = $result->fetch_array();

        if (!$row) {
            http_response_code(422);
            die('player does not exist'); // player with such ID doesn't exist in DB
        }
    } else {
        die($mysqli->error);
    }

    return $row['points'];
}

function deductFromPlayersBalance($playerIdString, $pointsToDeduct) {
    global $mysqli;

    $playerId = getPlayerIdFromString($playerIdString);

    if (!is_numeric($pointsToDeduct)) {
        http_response_code(422);
        die('points should be a numeric value');
    }

    $playerBalance = getPlayerBalance($playerIdString);

    if ($playerBalance < $pointsToDeduct) {
        die('player '.$playerId.' doesnt have enough points'); // player doesn't have enough points, balance can't go below 0
    }

    $sql = "UPDATE `players` SET points = points - ".$pointsToDeduct." WHERE player_id = ".intval($playerId);
    if ($mysqli->query($sql) === TRUE) {
        //
    } else {
        http_response_code(500);
        die($mysqli->error);
    }
}

function topUpPlayersBalance($playerIdString, $pointsToAdd) {
    global $mysqli;

    $playerId = getPlayerIdFromString($playerIdString);

    if (!is_numeric($pointsToAdd)) {
        http_response_code(422);
        die('points should be a numeric value');
    }

    $sql = "INSERT INTO `players` (player_id, points) 
            VALUES(".intval($playerId).", ".$pointsToAdd.") 
            ON DUPLICATE KEY 
            UPDATE points = points + ".$pointsToAdd;

    if ($mysqli->query($sql) === TRUE) {
        //
    } else {
        die($mysqli->error);
    }

}

function registerParticipantsInTournament($tournamentId, $playerIdString, $backerIdArray) {
    global $mysqli;

    $playerId = getPlayerIdFromString($playerIdString);
    $tournamentId = getTournamentId($tournamentId);

    $tournamentData = getTournamentData($tournamentId);
    if (!$tournamentData) {
        http_response_code(409);
        die('tournament is not announced'); // tournament is not announced
    }

    if ($tournamentData['closed'] === 'Y') {
        http_response_code(409);
        die('tournament is closed already'); // tournament is closed already
    }

    if (getPlayerOfTournament($tournamentId, $playerIdString)) {
        http_response_code(409);
        die('player '.$playerIdString.' already registered in tournament '.$tournamentId); // player with such ID already registered in tournament
    }

    // check that playerId and backerIds are unique
    $participantsArray = $backerIdArray;
    array_push($participantsArray, $playerIdString);

    if ( count($participantsArray) !== count(array_unique($participantsArray)) ) {
        http_response_code(409);
        die('Duplicating participant IDs'); // duplicating participant IDs
    }

    $buyIn = $tournamentData['deposit'] / count($participantsArray);

    // run checks
    foreach($participantsArray as $participantIdString) {
        $playerBalance = getPlayerBalance($participantIdString);

        if ($playerBalance < $buyIn) {
            http_response_code(409); // players balance is lower than buy-in
            die('player '.$participantIdString.' balance is '.$playerBalance.' lower than buy-in '.$buyIn);
        }
    }

    // deduct buy-in amount from player's balance
    deductFromPlayersBalance($playerIdString, $buyIn);

    // register player in tournament
    $sql = "INSERT INTO `tournament_players` (tournament_id, player_id)
            VALUES(".intval($tournamentId).", ".intval($playerId).")";

    if ($mysqli->query($sql) === TRUE) {
        $playerRegId = $mysqli->insert_id;
    } else {
        http_response_code(500);
        die($mysqli->error);
    }


    foreach($backerIdArray as $backerIdString) {
        setBackersofPlayer($playerRegId, $backerIdString);
        deductFromPlayersBalance($backerIdString, $buyIn);
    }
}

function setBackersofPlayer($playerRegId, $backerIdString) {
    global $mysqli;

    $backerId = getPlayerIdFromString($backerIdString);

    $sql = "INSERT INTO `backers` (player_reg_id, backer_id)
            VALUES(".intval($playerRegId).", ".intval($backerId).")";

    if ($mysqli->query($sql) === TRUE) {
        //
    } else {
        http_response_code(500);
        die($mysqli->error);
    }
}

function getBackers($playerRegId) {
    global $mysqli;

    $sql = "SELECT * FROM `backers` WHERE player_reg_id = ".intval($playerRegId);

    $backers = [];

    if ($result = $mysqli->query($sql)) {
        while($row = $result->fetch_assoc()) {
            array_push($backers, "P".$row['backer_id']);
        }
    } else {
        http_response_code(500);
        die($mysqli->error);
    }

    return $backers;
}

function getPlayerOfTournament($tournamentId, $playerIdString) {
    global $mysqli;

    $playerId = getPlayerIdFromString($playerIdString);
    $tournamentId = getTournamentId($tournamentId);

    $sql = "SELECT * FROM `tournament_players` WHERE tournament_id = ".intval($tournamentId)." AND player_id = ".intval($playerId);

    if ($result = $mysqli->query($sql)) {
        $player = $result->fetch_assoc();
    } else {
        http_response_code(500);
        die($mysqli->error);
    }

    return $player;
}

function announceTournament($tournamentId, $deposit) {
    global $mysqli;

    $tournamentId = getTournamentId($tournamentId);

    if (!is_numeric($deposit) || $deposit < 0) {
        http_response_code(422);
        die('deposit must be a numeric value');
    }

    $tournamentData = getTournamentData($tournamentId);
    if ($tournamentData) {
        die('Tournament with such ID is already announced'); // tournament with such ID is already announced
    }

    $sql = "INSERT INTO `tournaments` (tournament_id, deposit) 
            VALUES(".intval($tournamentId).", ".$deposit.")";

    if ($mysqli->query($sql) === TRUE) {
        //
    } else {
        http_response_code(500);
        die($mysqli->error);
    }
}

function closeTournament($tournamentId) {
    global $mysqli;

    $tournamentId = getTournamentId($tournamentId);

    $sql = "UPDATE `tournaments` SET closed = 'Y' WHERE tournament_id = ".intval($tournamentId);

    if ($mysqli->query($sql) === TRUE) {
        //
    } else {
        http_response_code(500);
        die($mysqli->error);
    }
}
